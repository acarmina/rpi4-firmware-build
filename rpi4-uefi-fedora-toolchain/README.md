# Build toolchain
```
$ sudo docker build -t build .
``` 
# Executes the build
```
$ sudo docker run -it --name buld_stuff2 --mount type=bind,source=`pwd`/src,target=/var/src build:latest
```
# Remote execution after having the docker image built
```
docker run -it --name buld_stuff2 --mount type=bind,source=`pwd`/src,target=/var/src build:latest
```

# podman
```
podman build -t build .
```

```
podman run -it --name buld_stuff  --security-opt label=disable  --mount type=bind,source=`pwd`/src,target=/var/tmp build:latest build.sh 012345 "pippo pippo" "https://gitlab.com/redhat14/edk2" "https://gitlab.com/redhat14/edk2-platforms" "https://gitlab.com/redhat14/edk2-non-osi" "https://gitlab.com/redhat14/arm-trusted-firmware"
```

```
podman run -it --rm -v `pwd`/src:/var/tmp:Z build:latest build.sh 012345 "pippo pippo" "https://gitlab.com/redhat14/edk2" "https://gitlab.com/redhat14/edk2-platforms" "https://gitlab.com/redhat14/edk2-non-osi" "https://gitlab.com/redhat14/arm-trusted-firmware"
```
# GitLab Runner
The container expects to run in a directory containing the subdirectory `src`
This subdirectory is used to place the last build artifacts and the gitlab runner configuration.

## GitLab Runner registration
registers the gitlab runner to the project
```
podman run -it --rm -v `pwd`/src:/var/tmp:Z  -v `pwd`/src:/etc/gitlab-runner:Z d7e764d08945 gitlab-runner register
```
Follow the wizard by entering the info in 
Settings -> CI/CD -> Runners -> Specific runners 

## GitLab Runner execution
execute the gitlab runner
```
podman run -it --rm -v `pwd`/src:/var/tmp:Z  -v `pwd`/src:/etc/gitlab-runner:Z d7e764d08945 gitlab-runner register
```
**warning**: the command runs the container in foreground.
