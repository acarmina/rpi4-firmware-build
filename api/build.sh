#!/bin/bash
[ $# -ne 6 ] && echo "build.sh <HASH> <COMMIT> <EDK2> <EDK2_PLATFORMS> <EDK2_NON_OSI> <ATF>" && exit 1
HASH=$1
COMMIT=$2
EDK2=$3
EDK2_PLATFORMS=$4
EDK2_NON_OSI=$5
ATF=$6

PWD_PRJ=`pwd`
SRC_HOME=${PWD_PRJ}/src/rpi4
ARTIFACTS=${PWD_PRJ}/artifacts
set -eo pipefail
rm -rf ${ARTIFACTS}
mkdir -p ${ARTIFACTS}
rm -rf ${SRC_HOME}
mkdir -p ${SRC_HOME} && cd ${SRC_HOME}
git clone ${EDK2} && cd edk2 && git submodule update --init && cd ..
git clone ${EDK2_PLATFORMS} && cd edk2-platforms && git submodule update --init && cd ..
git clone ${EDK2_NON_OSI}  && cd edk2-non-osi && git submodule update --init && cd ..
git clone ${ATF}
export WORKSPACE=${SRC_HOME} && export PACKAGES_PATH=${SRC_HOME}/edk2:${SRC_HOME}/edk2-platforms:${SRC_HOME}/edk2-non-osi && export PRELOADED_BL33_BASE=0x20000
make -C ${SRC_HOME}/edk2/BaseTools
echo "+++++++++++++++++++++++++++++++++"
cd ${SRC_HOME}/edk2
set --
. edksetup.sh
sed -i 's/^ACTIVE_PLATFORM/#ACTIVE_PLATFORM/' ${SRC_HOME}/edk2/Conf/target.txt && sed -i 's/^TARGET_ARCH/#TARGET_ARCH/' ${SRC_HOME}/edk2/Conf/target.txt &&
cd ${SRC_HOME}/edk2 && build -n $(getconf _NPROCESSORS_ONLN) -t GCC5 -p ${SRC_HOME}/edk2-platforms/Platform/RaspberryPi/RPi4/RPi4.dsc
cp ${PWD_PRJ}/src/rpi4/Build/RPi4/DEBUG_GCC5/FV/RPI_EFI.fd ${ARTIFACTS}/
cp ${PWD_PRJ}/src/rpi4/edk2-non-osi/Platform/RaspberryPi/RPi4/TrustedFirmware/bl31.bin ${ARTIFACTS}/
tar zcvf ${PWD_PRJ}/${HASH}_artifacts.tar.gz ${ARTIFACTS}/*
