package main
import (
	"fmt"
	"log"
	"net/http"
	"encoding/json"
	"github.com/gorilla/mux"
	"os/exec"
	"io/ioutil"
)

type GitRequest struct{
	Commit_Hash		*string `json:commit_hash`
	Commit_String		*string `json:commit_string`
	Git_Edk2		*string `json:git_edk2`
	Git_Edk2_Platforms	*string `json:git_edk2_platforms`
	Git_Edk2_Non_Osi	*string `json:git_edk2_non_osi`
	Git_ATF			*string `json:git_atf`
}

type Artifacts_req struct {
	Commit_Hash             *string `json:commit_hash`
}

func buildJob(w http.ResponseWriter, r *http.Request){
	var job GitRequest
	decoder := json.NewDecoder(r.Body)
	response:="error"
	resp_code:=500;


	err := decoder.Decode(&job)
	if err == nil {
		if (job.Commit_Hash!= nil) && (job.Commit_String!= nil) && (job.Git_Edk2!= nil) && (job.Git_Edk2_Platforms!= nil) && (job.Git_Edk2_Non_Osi!= nil) && (job.Git_ATF!= nil) {
			resp_code=200;
			log.Printf("Build job for commit hash %s commit string %s Started", *job.Commit_Hash, *job.Commit_String)
			cmd_str := fmt.Sprintf("./build.sh %s %s %s %s %s %s", *job.Commit_Hash, *job.Commit_String, *job.Git_Edk2, *job.Git_Edk2_Platforms, *job.Git_Edk2_Non_Osi, *job.Git_ATF)
			log.Printf("calling %s", cmd_str)
			cmd := exec.Command("/bin/bash", "-c", cmd_str)
			responseb, _ := cmd.CombinedOutput()
			response = string(responseb)
			log.Printf("Build job for commit hash %s commit string %s Ended", *job.Commit_Hash, *job.Commit_String)
			} else {
				log.Println("Missing parameters")
				}
	 	} else {
//			log.Println(err)
			log.Println("Malformed request")
			}
	w.WriteHeader(resp_code)
	fmt.Fprintf(w, response)
}


func fetchArtifacts(w http.ResponseWriter, r *http.Request){
	var req Artifacts_req
	decoder := json.NewDecoder(r.Body)

	err := decoder.Decode(&req)
	if err == nil {
		if (req.Commit_Hash!= nil) {
			fn:= fmt.Sprintf("%s_artifacts.tar.gz", *req.Commit_Hash);
			log.Printf("Fetching %s", fn)
			streambytes, err := ioutil.ReadFile(fn)
			if err == nil {
				w.Header().Set("Content-type", "application/gzip")
				log.Printf("Send %s back", fn)
				w.WriteHeader(200)
				w.Write(streambytes)
				return
				}
			log.Printf("%s not found", fn)
			} else {
				log.Println("Missing parameters")
				}
	 	} else {
			log.Println("Malformed request")
			}
	w.WriteHeader(404)
	fmt.Fprintf(w, "file not found")
}

func homePage(w http.ResponseWriter, r *http.Request){
	fmt.Fprintf(w, "Nothing here!")
}

func handleRequest(){

	router:= mux.NewRouter().StrictSlash(true)

	router.HandleFunc("/", homePage).Methods("GET") 
	router.HandleFunc("/build_job", buildJob).Methods("POST")
	router.HandleFunc("/artifacts", fetchArtifacts).Methods("POST")
	log.Fatal(http.ListenAndServe(":1234", router))
}

func main(){
	handleRequest()
}
