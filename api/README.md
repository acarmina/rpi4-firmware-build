# API server
The `main` executable needs to be in the same directory as `build.sh` script.
Two json http API are provided:
* `/build_job`: it expect 6 arguments: `commit_hash`, `commit_string`, `git_edk2`, `git_edk2_platforms`, `git_edk2_non_osi` and `git_atf`
* `/artifacts`: it expects only one argument: 'commit_hash`

# Gitlab CI/CD script example

```
stages:
  - build
  - test
build:   
  stage: build   
  script: >
    - wget --header="Content-Type: application/json" --post-data="{\"commit_hash\":\"$CI_COMMIT_SHA\", \"commit_string\":\"pippo\", \"git_edk2\":\"https://gitlab.com/acarmina/edk2.git\", \"git_edk2_platforms\":\"https://gitlab.com/acarmina/edk2-platforms.git\", \"git_edk2_non_osi\":\"https://gitlab.com/acarmina/edk2-non-osi.git\", \"git_atf\":\"https://gitlab.com/acarmina/arm-trusted-firmware.git\"}" -O - -q http://172.31.8.184:1234/build_job
    - wget -header="Content-Type: application/json" --post-data="{ \"commit_hash\":\"$CI_COMMIT_SHA\" }" -O ${CI_COMMIT_SHA}_artifacts.tar.gz -q http://172.31.8.184:1234/artifacts
  artifacts:
      paths:
          - ${CI_COMMIT_SHA}_artifacts.tar.gz

```

